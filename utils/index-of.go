package utils

func IndexOf(slice []string, item string) int {
	for index, current := range slice {
		if current == item {
			return index
		}
	}
	return -1
}
